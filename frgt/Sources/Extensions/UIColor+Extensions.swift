//
//  UIColor+Extensions.swift
//  BambooN
//
//  Created by Yurii Dukhovnyi on 2/6/19.
//  Copyright © 2019 Dukhovnyi Family. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

    public convenience init(_ hex6: UInt32, alpha: CGFloat = 1) {
        let divisor = CGFloat(255)
        let red     = CGFloat((hex6 & 0xFF0000) >> 16) / divisor
        let green   = CGFloat((hex6 & 0x00FF00) >>  8) / divisor
        let blue    = CGFloat( hex6 & 0x0000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
