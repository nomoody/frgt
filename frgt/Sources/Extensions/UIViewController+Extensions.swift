//
//  UIViewController+Extensions.swift
//  BambooN
//
//  Created by Yurii Dukhovnyi on 1/15/19.
//  Copyright © 2019 Dukhovnyi Family. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func add(_ child: UIViewController, to containerView: UIView? = nil) {

        addChild(child)
        (containerView ?? view).addSubview(child.view)
        child.didMove(toParent: self)
    }

    func remove() {

        // Just to be safe, we check that this view controller
        // is actually added to a parent before removing it.
        guard parent != nil else {
            return
        }

        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}

extension UIViewController {

    /// Instantiates view controller from nib.
    /// NB! Nib name should be the same as view controller name.
    ///
    static func instantiateFromNib<T>() -> T? {

        let nib = UINib(nibName: String(describing: T.self), bundle: nil)
        let nibArray = nib.instantiate(withOwner: nil, options: nil)

        return nibArray.first as? T
    }
}
