//
//  Swinject+Extensions.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 24/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import Swinject

extension Container {

    struct Default {
        static let container = Container()
        static let resolver = container.synchronize()
    }
}
