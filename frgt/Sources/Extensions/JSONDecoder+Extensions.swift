//
//  JSONDecoder+Extensions.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 06/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation

extension JSONDecoder {

    static func makeEncoderWithDateFormatter() -> JSONDecoder {

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }

    // MARK: - Private
}
