//
//  RootNavigator.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 23/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import RxSwift
import Swinject
import UIKit

private let logger = Logger()

final class RootNavigator: Navigator {

    enum Destination {
        case authorization
        case authorizedArea
    }

    init(viewController: UIViewController, container: Container = Container.Default.container) {

        logger.debug("RootNavigator has been constructed.")
        self.rootViewController = viewController
        self.container = container
        self.userController = container.resolve(UserController.Interface.self)!
    }

    deinit {
        logger.debug("~RootNavigator has been destructed.")
    }

    func dismiss() {
        preconditionFailure("Unexpected `dismiss` call for RootNavigator.")
    }

    func present() {

        subscription.disposable = userController.state

            .asDriver(onErrorJustReturn: .idle)
            .drive(onNext: { [weak self] newState in
                logger.debug("UserController state has been changed to '\(newState)'.")

                switch newState {

                case .notAuthorized:
                    self?.navigate(to: .authorization)

                case .authorized:
                    self?.navigate(to: .authorizedArea)

                case .idle:
                    self?.child?.dismiss()

                case .noConnection:
                    guard let alert = self?.alert else {
                        return
                    }

                    self?.rootViewController?.present(alert, animated: true)
                }
            })
    }

    func navigate(to destination: Destination) {

        guard let rootViewController = rootViewController else {

            logger.error("Can navigate to '\(destination)' due to 'rootViewController' is nil.")
            return
        }

        child?.dismiss(); defer { child?.present() }

        switch destination {

        case .authorization:
            let navigator = AuthNavigator(with: rootViewController, userController: userController)
            child = navigator

        case .authorizedArea:
            let navigator = AppNavigator(with: rootViewController, container: container)
            child = navigator
        }
    }

    // MARK: - Private

    private let userController: UserController.Interface

    private weak var rootViewController: UIViewController?
    private let container: Container
    private let subscription = SerialDisposable()
    private var child: Navigator?

    private lazy var alert: UIAlertController = {

        let alert = UIAlertController(title: "Havah",
                                      message: "Can't connect to server. Please try again later.",
                                      preferredStyle: .alert)

        let action = UIAlertAction(title: "Try again", style: .default, handler: { [weak self] action in

            self?.userController.update()
        })

        alert.addAction(action)

        return alert
    }()
}
