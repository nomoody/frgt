//
//  Coordinator.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 23/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import UIKit

protocol Navigator: class {

    func present()
    func dismiss()
}
