//
//  PreferencesNavigator.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 12/05/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import UIKit

class PreferencesNavigator: Navigator {

    enum Destination {
        case main
    }

    weak var navigationController: UINavigationController?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func present() {

        navigate(to: .main)
    }

    func dismiss() {

        navigationController?.remove()
    }

    func navigate(to destination: Destination) {

        navigationController?.pushViewController(makeViewController(for: destination), animated: true)
    }

    // MARK: - Private

    private let storyboard = UIStoryboard(name: "Preferences", bundle: nil)
    private func makeViewController(for destination: Destination) -> UIViewController {

        switch destination {
        case .main:
            let viewController = storyboard.instantiateViewController(withIdentifier: PreferencesViewController.storyboardId) as! PreferencesViewController
            viewController.navigator = self
            return viewController
        }
    }
}
