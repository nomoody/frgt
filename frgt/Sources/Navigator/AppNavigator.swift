//
//  MainNavigator.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 23/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Swinject
import UIKit

final class AppNavigator: Navigator {

    init(with viewController: UIViewController, container: Container) {
        self.rootViewController = viewController
    }

    func present() {
        rootViewController?.add(appNavigationController)
    }

    func dismiss() {
        appNavigationController.remove()
    }

    // MARK: - Private

    private weak var rootViewController: UIViewController?
    private let storyboard = UIStoryboard(name: "Main", bundle: nil)

    private lazy var appNavigationController: AppNavigationController = {

        let storyboard = UIStoryboard(name: "AppNavigation", bundle: nil)
        return storyboard.instantiateInitialViewController() as! AppNavigationController
    }()
}
