//
//  NotesNavigator.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 07/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Swinject
import UIKit

final class NotesNavigator: Navigator {

    enum Destination {
        case list
        case create
        case edit(_ note: Note)
    }

    weak var navigationController: UINavigationController?

    let viewModel: NotesViewModel.Interface

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.viewModel = Container.Default.resolver.resolve(NotesViewModel.Interface.self)!
    }

    func present() {

        navigate(to: .list)
    }

    func dismiss() {

        navigationController?.remove()
    }

    func navigate(to destination: Destination) {

        switch destination {
        case .list:
            if let vc: NotesViewController = makeViewController(with: NotesViewController.storyboardId) {
                vc.navigator = self
                navigationController?.pushViewController(vc, animated: true)
            }

        case .create:
            if let vc: EditNoteViewController = makeViewController(with: EditNoteViewController.storyboardId) {
                vc.navigator = self
                navigationController?.pushViewController(vc, animated: true)
            }

        case let .edit(note):
            if let vc: EditNoteViewController = makeViewController(with: EditNoteViewController.storyboardId) {
                vc.navigator = self
                vc.note = note
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

    // MARK: - Private

    private let storyboard = UIStoryboard(name: "Notes", bundle: nil)

    private func makeViewController<VC>(with identifier: String) -> VC? {
        return storyboard.instantiateViewController(withIdentifier: identifier) as? VC
    }
}
