//
//  AuthNavigator.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 23/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import RxCocoa
import RxSwift
import Swinject
import UIKit

final class AuthNavigator: Navigator {

    enum Destination {
        case authorization
    }

    init(with viewController: UIViewController, userController: UserController.Interface) {
        self.rootViewController = viewController
        self.userController = userController
    }

    func present() {

        navigate(to: .authorization)
    }

    func dismiss() {

        presentedViewController?.remove()
    }

    func navigate(to destination: Destination) {

        switch destination {
        case .authorization:
            let identifier = AuthorizationViewController.storyboardId
            guard let viewController = storyboard.instantiateViewController(withIdentifier: identifier) as? AuthorizationViewController else {
                return
            }

            viewController.navigator = self
            rootViewController?.add(viewController)
            presentedViewController = viewController
        }
    }

    func signInWith(email: String, password: String) -> Completable {

        return userController.signInWith(email: email, password: password)
    }

    func signUpWith(_ email: String, password: String) -> Completable {

        return userController.signUpWith(email, password: password)
    }

    // MARK: - Private

    private weak var rootViewController: UIViewController?
    private weak var presentedViewController: UIViewController?
    private let storyboard = UIStoryboard(name: "Authorization", bundle: nil)

    private let userController: UserController.Interface
}
