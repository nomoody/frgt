//
//  AppNavigationController.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 28/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import UIKit

class AppNavigationController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupMenu()

        present(item: .notes)
    }

    // MARK: - IBOutlets

    @IBOutlet private weak var contentContainer: UIView?

    @IBOutlet private weak var menuContainer: UIView?
    @IBOutlet private weak var leadingMenuConstraing: NSLayoutConstraint?

    // MARK: - Private

    private let menuBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu-icon"), style: .plain,
                                                    target: self, action: #selector(menuBarButtonItemTouchUpInside))
    private var isMenuShowed = false
    private var runningAnimators = [UIViewPropertyAnimator]()

    private var currentNavigationController: UIViewController?
    private var notesNavigator: NotesNavigator?

    private var activeNavigator: Navigator?

    @objc
    private func menuBarButtonItemTouchUpInside() {
        prepareAnimators(isShowingAnimator: true)
        runAnimators()
    }

    private func present(item: MenuViewController.MenuItem) {

        activeNavigator?.dismiss()

        let navigationController = makeNavigationController()

        switch item {
        case .notes:
            activeNavigator = NotesNavigator(navigationController: navigationController)
            add(navigationController, to: contentContainer)

        case .preferences:
            activeNavigator = PreferencesNavigator(navigationController: navigationController)
            add(navigationController, to: contentContainer)
        }

        activeNavigator?.present()
        navigationController.viewControllers.first?.navigationItem.leftBarButtonItem = menuBarButtonItem
    }
}

extension AppNavigationController: MenuViewControllerDelegate {

    func menuViewControllerWantsToHide(_ menuViewController: MenuViewController) {

        guard runningAnimators.isEmpty else { return }

        prepareAnimators(isShowingAnimator: false)
        runAnimators()
    }

    func menuViewController(_ menuViewController: MenuViewController, didSelect item: MenuViewController.MenuItem) {

        present(item: item)
        menuViewControllerWantsToHide(menuViewController)
    }

    private func setupMenu() {

        let vc = storyboard?.instantiateViewController(
            withIdentifier: MenuViewController.storyboardId
        ) as! MenuViewController

        vc.delegate = self
        add(vc, to: menuContainer)
    }

    private func prepareAnimators(isShowingAnimator: Bool) {

        let showAnimator = UIViewPropertyAnimator(duration: 0.3, curve: .easeOut) { [weak self] in

            self?.updateMenuLeadingConstraint(isShown: isShowingAnimator)
            self?.view.layoutIfNeeded()
        }

        showAnimator.addCompletion { [weak self] position in
            switch position {
            case .start:
                self?.updateMenuLeadingConstraint(isShown: !isShowingAnimator)

            case .end:
                self?.isMenuShowed.toggle()
                self?.updateMenuLeadingConstraint(isShown: isShowingAnimator)

            case .current:
                guard showAnimator.fractionComplete > 0.5 else {
                    self?.updateMenuLeadingConstraint(isShown: isShowingAnimator)
                    return
                }

                self?.updateMenuLeadingConstraint(isShown: !isShowingAnimator)
                self?.isMenuShowed.toggle()

            @unknown default:
                fatalError("Unexpected position.")
            }

            self?.runningAnimators.removeAll()
        }

        runningAnimators.append(showAnimator)
    }

    private func updateMenuLeadingConstraint(isShown: Bool) {
        self.leadingMenuConstraing?.constant = isShown ? -(self.menuContainer?.frame.width ?? 0) : 0
    }

    private func runAnimators() {
        runningAnimators.forEach { $0.startAnimation() }
    }
}

extension AppNavigationController {

    private func makeNavigationController() -> UINavigationController {

        let navigationController = UINavigationController()
        navigationController.navigationBar.barTintColor = Appearance.shared.backgroundColor
        navigationController.navigationBar.tintColor = Appearance.shared.textColor
        navigationController.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        return navigationController
    }
}
