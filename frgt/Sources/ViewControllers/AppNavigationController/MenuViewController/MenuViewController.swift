//
//  MenuViewController.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 28/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import UIKit

protocol MenuViewControllerDelegate: class {
    func menuViewController(_ menuViewController: MenuViewController, didSelect item: MenuViewController.MenuItem)
    func menuViewControllerWantsToHide(_ menuViewController: MenuViewController)
}

class MenuViewController: UIViewController {

    static let storyboardId = String(describing: MenuViewController.self)

    enum MenuItem: CaseIterable {
        case notes, preferences

        var title: String {

            switch self {

            case .notes:        return "Notes"
            case .preferences:  return "Preferences"
            }
        }
    }

    weak var delegate: MenuViewControllerDelegate?

    // MARK: - IBOutlets

    @IBOutlet private weak var accountImageView: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Private

    private let dataSource = MenuItem.allCases

    @objc private func hideMenu() {
        delegate?.menuViewControllerWantsToHide(self)
    }
}

extension MenuViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: MenuTableViewCell.reuseIdentifier)
            as? MenuTableViewCell
        cell?.titleLabel?.text = dataSource[indexPath.row].title.uppercased()

        return cell ?? UITableViewCell()
    }
}

extension MenuViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        delegate?.menuViewController(self, didSelect: dataSource[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
