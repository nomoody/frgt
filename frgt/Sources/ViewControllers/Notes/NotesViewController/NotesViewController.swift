//
//  NotesViewController.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 08/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import RxSwift
import Swinject
import UIKit

private let logger = Logger()

class NotesViewController: UIViewController {

    static let storyboardId = String(describing: NotesViewController.self)

    weak var navigator: NotesNavigator?

    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)

        logger.debug("NotesViewController has been constructed.")
    }

    deinit {
        logger.debug("~NotesViewController has been destructed.")
    }

    override func viewDidLoad() {

        super.viewDidLoad()

        tableView?.rowHeight = UITableView.automaticDimension

        handleViewModelChanges()
        navigationItem.rightBarButtonItem = createNoteBarButtonItem

        refreshControl.addTarget(self, action: #selector(reloadNotes), for: .valueChanged)
        tableView?.addSubview(refreshControl)

        title = "Notes"
        reloadNotes()
    }

    // MARK: - IBOutlets

    @IBOutlet private weak var tableView: UITableView?
    @IBOutlet private weak var createNoteBarButtonItem: UIBarButtonItem?

    @IBAction private func createNoteButtonTouchUpInside(_ sender: UIBarButtonItem) {

        navigator?.navigate(to: .create)
    }

    // MARK: - Private

    private let disposeBag = DisposeBag()
    private let subscription = SerialDisposable()
    private var dataSource = [Note]()

    private let refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = Appearance.shared.textColor
        return refreshControl
    }()

    @objc private func reloadNotes() {

        refreshControl.beginRefreshing()
        subscription.disposable = navigator?.viewModel.reload()
            .observeOn(MainScheduler.instance)
            .subscribe(onCompleted: { [weak self] in self?.refreshControl.endRefreshing() }) { [weak self] error in
                self?.refreshControl.endRefreshing()
                print(error)
        } ?? Disposables.create()
    }

    private func handleViewModelChanges() {

        navigator?.viewModel.changes
            .drive(onNext: { [weak self] change in

                switch change {

                case let .reloadAll(notes):
                    self?.dataSource.removeAll()
                    self?.dataSource.append(contentsOf: notes)
                    self?.tableView?.reloadData()

                case let .insert(note):
                    self?.tableView?.beginUpdates()
                    self?.dataSource.insert(note, at: 0)
                    self?.tableView?.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                    self?.tableView?.endUpdates()

                case let .update(note):
                    self?.tableView?.beginUpdates()
                    guard let index = self?.dataSource.firstIndex(where: { $0.id == note.id }) else { return }
                    self?.dataSource[index] = note
                    if let cell = self?.tableView?.cellForRow(at: IndexPath(row: index, section: 0)) as? NoteTableViewCell {
                        cell.update(with: note)
                    }
                    self?.tableView?.endUpdates()

                case let .delete(note):
                    self?.tableView?.beginUpdates()
                    guard let index = self?.dataSource.firstIndex(where: { $0.id == note.id }) else { return }
                    self?.dataSource.remove(at: index)
                    self?.tableView?.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                    self?.tableView?.endUpdates()
                }

            })
        .disposed(by: disposeBag)
    }
}

extension NotesViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: NoteTableViewCell.reuseIdentifier)
            as? NoteTableViewCell
        cell?.update(with: dataSource[indexPath.row])

        return cell ?? UITableViewCell()
    }
}

extension NotesViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let note = dataSource[indexPath.row]
        navigator?.navigate(to: .edit(note))
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return [
            UITableViewRowAction(style: .destructive, title: "Delete", handler: { [weak self] action, indexPath in

                guard let self = self else { return }
                self.deleteNote(self.dataSource[indexPath.row])
            })
        ]
    }

    // MARK: - Private

    private func deleteNote(_ note: Note) {

        let alert = UIAlertController(title: "Delete", message: "Are you sure want to delete note: \(note.content.prefix(30))", preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: "I'm sure. Delete.", style: .destructive) { [weak self] action in
            self?.subscription.disposable = self?.navigator?.viewModel
                .delete(note: note)
                .subscribe() ?? Disposables.create()
        }
        alert.addAction(deleteAction)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(alert, animated: true)
    }
}
