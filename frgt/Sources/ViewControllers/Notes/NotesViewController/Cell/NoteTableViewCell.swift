//
//  NoteTableViewCell.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 21/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {

    static let reuseIdentifier = String(describing: NoteTableViewCell.self)

    func update(with note: Note) {

        contentLabel?.text = note.content
        updatedAtLabel?.text = makeDateFormatter(for: note.updatedAt).string(from: note.updatedAt)
    }

    // MARK: - IBOutlets

    @IBOutlet private weak var contentLabel: UILabel?
    @IBOutlet private weak var updatedAtLabel: UILabel?

    // MARK: - Private

    private func makeDateFormatter(for date: Date) -> DateFormatter {

        let df = DateFormatter()
        if Calendar.current.isDateInToday(date) {
            df.timeStyle = .short
            df.dateStyle = .none
        } else {
            df.timeStyle = .none
            df.dateStyle = .short
        }

        return df
    }
}
