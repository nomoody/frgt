//
//  NotesModel+Factory.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 19/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import RxSwift
import Swinject

extension NotesModel {

    final class Factory {

        static func register(into container: Container) {

            let resolver = container.synchronize()
            container.register(Interface.self) { _ in
                return Impl(resolver: resolver)
            }
        }
    }

    private class Impl: Interface {

        init(resolver: Resolver) {
            self.resolver = resolver
            self.userController = resolver.resolve(UserController.Interface.self)!
            self.networkController = resolver.resolve(NetworkController.Interface.self)!
        }

        func getAll() -> Single<[Note]> {

            var request = NetworkController.URLBuilder.getAllNotes()
            userController.signRequest(&request)

            return networkController.send(request: request)
                .map { try Note.Decoder.decode([Note].self, from: $0) }
        }

        func createNote(with content: String) -> Single<Note> {

            var request = NetworkController.URLBuilder.saveNote(content: content)
            userController.signRequest(&request)

            return networkController.send(request: request)
                .map { try Note.Decoder.decode(Note.self, from: $0) }
        }

        func updateNote(_ note: Note, with content: String) -> Single<Note> {

            var request = NetworkController.URLBuilder.updateNote(note, with: content)
            userController.signRequest(&request)

            return networkController.send(request: request)
                .map {
                    try Note.Decoder.decode(Note.self, from: $0)
            }
        }

        func deleteNote(_ note: Note) -> Completable {

            var request = NetworkController.URLBuilder.deleteNote(note)
            userController.signRequest(&request)

            return networkController.send(request: request)
                .asCompletable()
        }

        // MARK: - Private

        private let resolver: Resolver
        private let userController: UserController.Interface
        private let networkController: NetworkController.Interface
    }
}
