//
//  NotesModel.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 08/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import RxSwift

struct NotesModel {

    typealias Interface = NotesModelInterface
}

protocol NotesModelInterface {

    func getAll() -> Single<[Note]>

    func createNote(with content: String) -> Single<Note>
    func updateNote(_ note: Note, with content: String) -> Single<Note>
    func deleteNote(_ note: Note) -> Completable
}
