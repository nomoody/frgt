//
//  NotesViewModel+Factory.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 08/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import Swinject

extension NotesViewModel {

    final class Factory {

        static func register(into container: Container) {

            let resolver = container.synchronize()
            container.register(Interface.self) { _ in
                return Impl(resolver: resolver)
            }

            NotesModel.Factory.register(into: container)
        }
    }

    private class Impl: Interface {

        init(resolver: Resolver) {

            self.model = resolver.resolve(NotesModel.Interface.self)!
        }

        var changes: Driver<NotesViewModel.Change> {
            return changesSubj.asDriver(onErrorJustReturn: .reloadAll([]))
        }

        func reload() -> Completable {
            return model.getAll()

                .map { [weak self] notes in
                    let sortedNotes = notes.sorted(by: { $0.updatedAt > $1.updatedAt })
                    self?.changesSubj.onNext(.reloadAll(sortedNotes))
                }
                .asCompletable()
        }

        func update(_ note: Note, content: String) -> Completable {

            return model.updateNote(note, with: content)
                .do(onSuccess: { [weak self] note in

                    self?.changesSubj.onNext(.update(note))
                })
                .asCompletable()
        }

        func create(with content: String) -> Completable {
            return model.createNote(with: content)
                .do(onSuccess: { [weak self] note in
                    self?.changesSubj.onNext(.insert(note))
                })
                .asCompletable()
        }

        func delete(note: Note) -> Completable {
            return model.deleteNote(note)
                .do { [weak self] in
                    self?.changesSubj.onNext(.delete(note))
            }
        }

        // MARK: - Private

        private let model: NotesModel.Interface
        private let changesSubj = PublishSubject<Change>()
    }
}
