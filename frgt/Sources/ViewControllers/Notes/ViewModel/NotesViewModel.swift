//
//  NotesViewModel.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 08/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

struct NotesViewModel {

    typealias Interface = NotesViewModelInterface

    enum Change {
        case reloadAll(_ notes: [Note])
        case insert(_ note: Note)
        case update(_ note: Note)
        case delete(_ note: Note)
    }
}

protocol NotesViewModelInterface {

    var changes: Driver<NotesViewModel.Change> { get }

    func reload() -> Completable
    func update(_ note: Note, content: String) -> Completable
    func create(with content: String) -> Completable
    func delete(note: Note) -> Completable
}
