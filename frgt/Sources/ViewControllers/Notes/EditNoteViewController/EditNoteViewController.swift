//
//  CreateNoteViewController.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 21/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import RxSwift
import Swinject
import UIKit

private let logger = Logger()

class EditNoteViewController: UIViewController {

    static let storyboardId = String(describing: EditNoteViewController.self)

    weak var navigator: NotesNavigator?
    var note: Note?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        logger.debug("EditNoteViewController has been constructed.")
    }

    deinit {
        logger.debug("~EditNoteViewController has been destructed.")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        updateAppearanceSubj.onNext(())
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = saveBarButtonItem
        textView?.text = note?.content

        registerKeyboardNotifications()

        textView?.font = Appearance.shared.editor.font.regular

        updateAppearanceSubj
            .throttle(1, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] content in

            guard let textView = self?.textView else {
                return
            }
            self?.updateTextView(textView)
        })
        .disposed(by: disposeBag)
    }

    // MARK: - IBOutlets & IBActions

    @IBOutlet private weak var textView: UITextView?
    @IBOutlet private weak var bottomTextViewCostraint: NSLayoutConstraint?

    @IBOutlet private weak var saveBarButtonItem: UIBarButtonItem?

    @IBAction private func saveButtonTouchUpInside(_ sender: UIBarButtonItem) {

        guard let content = textView?.text else { return }

        if note == nil {
            createNote(with: content)
        } else {
            updateNote(with: content)
        }
    }

    // MARK: - Private

    private let disposeBag = DisposeBag()
    private let subscription = SerialDisposable()
    private let updateAppearanceSubj = PublishSubject<Void>()
    private let pen = Pen()
    
    private func createNote(with content: String) {

        subscription.disposable = navigator?.viewModel.create(with: content)
            .observeOn(MainScheduler.instance)
            .subscribe(onCompleted: { [weak self] in
                self?.navigationController?.popViewController(animated: true)
                }, onError: { error in
                    print(error.localizedDescription)
            }) ?? Disposables.create()
    }

    private func updateNote(with content: String) {

        guard let note = note else { return }

        subscription.disposable = navigator?.viewModel.update(note, content: content)
            .observeOn(MainScheduler.instance)
            .subscribe(onCompleted: { [weak self] in
                self?.navigationController?.popViewController(animated: true)
                }, onError: { error in
                    print(error.localizedDescription)
            }) ?? Disposables.create()
    }

    func updateTextView(_ textView: UITextView) {

        let textRange = textView.selectedTextRange
        defer { textView.selectedTextRange = textRange }
        
        let markers = try? pen.highlight(textView.text)
        let attributes = NSMutableAttributedString(string: textView.text)

        attributes.addAttributes([NSAttributedString.Key.foregroundColor : Appearance.shared.textColor,
                                  .font: Appearance.shared.editor.font.regular], range: NSRange(location: 0, length: textView.text.count))

        markers?.forEach { marker, ranges in

            ranges.forEach { range in

                attributes.addAttributes(marker.attributes, range: range)
            }
        }

        textView.attributedText = attributes
    }
}

extension EditNoteViewController {

    private func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    @objc func keyboardWillShow(notification: NSNotification) {

        guard let userInfo = notification.userInfo,
            let keyBoardValueBegin = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let keyBoardValueEnd = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue, keyBoardValueBegin != keyBoardValueEnd else {
                return
        }

        let keyboardHeight = keyBoardValueEnd.height

        bottomTextViewCostraint?.constant = keyboardHeight
    }
}

extension EditNoteViewController: UITextViewDelegate {

    func textViewDidChange(_ textView: UITextView) {

        updateAppearanceSubj.onNext(())
    }
}

extension Pen.Marker {

    var attributes: [NSAttributedString.Key: Any] {

        switch self {
        case .bold:
            return [
                NSAttributedString.Key.foregroundColor: Appearance.shared.textColor,
                NSAttributedString.Key.font: Appearance.shared.editor.font.bold,
            ]

        case .light:
            return [
                NSAttributedString.Key.foregroundColor: Appearance.shared.textColor,
                NSAttributedString.Key.font: Appearance.shared.editor.font.light,
            ]

        case .highlighted:
            return [
                NSAttributedString.Key.backgroundColor: Appearance.shared.textColor,
                NSAttributedString.Key.foregroundColor: Appearance.shared.highlightedTextColor,
                NSAttributedString.Key.font: Appearance.shared.editor.font.regular,
            ]

        case .paragraph:
            let paragraph = NSMutableParagraphStyle()
            paragraph.paragraphSpacingBefore = 30
            return [
                NSAttributedString.Key.paragraphStyle: paragraph,
                NSAttributedString.Key.font: Appearance.shared.editor.font.regular,
            ]
        }
    }
}

struct Pen {

    enum Marker: Hashable, CaseIterable {

        case bold
        case light
        case highlighted

        case paragraph

        var open: String {
            switch self {
            case .bold:
                return "*"
            case .light:
                return "`"
            case .highlighted:
                return "#"
            case .paragraph:
                return "^###"
            }
        }

        var close: String {
            switch self {
            case .bold:
                return "*"
            case .light:
                return "`"
            case .highlighted:
                return "#"
            case .paragraph:
                return "$"
            }
        }

        var regExpPattern: String {
            if self == .paragraph { return "^###(.+?)$" }
            return "\\\(open)(.+?)\\\(close)"
        }
    }

    init(markers: [Marker] = Marker.allCases) {
        self.markers = markers
    }

    func highlight(_ text: String) throws -> [Marker: [NSRange]] {

        var result = [Marker: [NSRange]]()

        try markers.forEach { marker in

            var ranges = [NSRange]()
            let regExp = try NSRegularExpression(pattern: marker.regExpPattern, options: .anchorsMatchLines)

            let range = NSRange(location: 0, length: text.count)
            regExp.enumerateMatches(in: text, options: .reportProgress , range: range) { result, flag, flag2 in
                if let range = result?.range {
                    ranges.append(range)
                }
            }

            result[marker] = ranges
        }

        return result
    }

    // MARK: - Private

    private let markers: [Marker]
}
