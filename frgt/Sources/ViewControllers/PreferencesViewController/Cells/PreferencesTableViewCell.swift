//
//  PreferencesTableViewCell.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 01/05/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import UIKit

class PreferencesTableViewCell: UITableViewCell {

    static let reuseIdentifier = String(describing: PreferencesTableViewCell.self)

    // MARK: - IBOutlets

    @IBOutlet weak var titleLabel: UILabel?
}
