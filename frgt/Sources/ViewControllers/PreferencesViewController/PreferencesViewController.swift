//
//  PreferencesViewController.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 01/05/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import RxSwift
import Swinject
import UIKit

private let logger = Logger()

class PreferencesViewController: UIViewController {

    static let storyboardId = String(describing: PreferencesViewController.self)

    weak var navigator: PreferencesNavigator?

    required init?(coder aDecoder: NSCoder) {

        self.userController = Container.Default.resolver.resolve(UserController.Interface.self)!

        super.init(coder: aDecoder)
        logger.debug("PreferencesViewController has been constructed.")
    }

    deinit {
        logger.debug("~PreferencesViewController has been destructed.")
    }

    // MARK: - IBOutlets

    @IBOutlet private weak var tableView: UITableView?

    // MARK: - Private

    private enum MenuItem: CaseIterable {
        case logOut

        var title: String {
            switch self {
            case .logOut:       return "Log Out"
            }
        }
    }

    private let dataSource = MenuItem.allCases
    private let userController: UserController.Interface
    private let subscription = SerialDisposable()
}

extension PreferencesViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return dataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: PreferencesTableViewCell.reuseIdentifier)
            as? PreferencesTableViewCell
        cell?.titleLabel?.text = dataSource[indexPath.row].title
        
        return cell ?? UITableViewCell()
    }
}

extension PreferencesViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        switch dataSource[indexPath.row] {
        case .logOut:
            subscription.disposable = userController.signOut()
                .subscribe { error in
                    Informator().show(.error(error))
            }
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }
}
