//
//  SignInViewController.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 23/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import RxSwift
import UIKit

private let logger = Logger()

class AuthorizationViewController: UIViewController {

    static let storyboardId = String(describing: AuthorizationViewController.self)
    weak var navigator: AuthNavigator?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        logger.debug("AuthorizationViewController has been constructed.")
    }

    deinit {
        logger.debug("~AuthorizationViewController has been destructed.")
    }

    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)

        updateUI()
    }

    // MARK: - IBOutlets

    @IBOutlet private weak var emailTextField: UITextField?
    @IBOutlet private weak var passwordTextField: UITextField?
    @IBOutlet private weak var signInButton: UIButton?
    @IBOutlet private weak var signInUpSwitcher: UIButton?

    // MARK: - IBActions

    private enum Error: Swift.Error, LocalizedError {
        case credentialAreNotFilled

        var errorDescription: String? {
            switch self {
            case .credentialAreNotFilled:
                return "Please check your credentials and try again."
            }
        }
    }

    @IBAction func signInButtonTouchUpInside(_ sender: UIButton?) {

        guard let email = emailTextField?.text, !email.isEmpty,
            let password = passwordTextField?.text, !password.isEmpty else {
            return handleError(Error.credentialAreNotFilled)
        }

        state == .signIn ? signInWith(email, password: password) :
            signUpWith(email, password: password)
    }

    @IBAction func signInUpSwitcherTouchUpInside(_ sender: UIButton?) {

        state = state == .signIn ? .signUp : .signIn
    }

    // MARK: - Private

    private enum State {
        case signIn, signUp

        var buttonTitle: String { return self == .signIn ? "Sign In" : "Sign Up" }
    }

    private let subscription = SerialDisposable()
    private lazy var textFields: [UITextField?] = { [emailTextField, passwordTextField] }()

    private var state = State.signIn { didSet { updateUI() } }

    private func updateUI() {
        emailTextField?.attributedPlaceholder = NSAttributedString(string: "email",
                                                                   attributes: [.foregroundColor: UIColor.lightGray])
        passwordTextField?.attributedPlaceholder = NSAttributedString(string: "password",
                                                                      attributes: [.foregroundColor: UIColor.lightGray])
        signInButton?.setTitle(state.buttonTitle, for: .normal)
        signInUpSwitcher?.setTitle(state == .signIn ? "Have no account yet? Sign Up here.": "Have an account already? Sign In here.",
                                   for: .normal)
    }

    private func signInWith(_ email: String, password: String) {

        let message = Informator().show(.activityIndicator)

        subscription.disposable = navigator?.signInWith(email: email, password: password)
            .observeOn(MainScheduler.instance)
            .do { message?.dismiss() }
            .subscribe(onError: { [weak self] error in

                self?.handleError(error)

            }) ?? Disposables.create()
    }

    private func signUpWith(_ email: String, password: String) {

        let message = Informator().show(.activityIndicator)

        subscription.disposable = navigator?.signUpWith(email, password: password)
            .observeOn(MainScheduler.instance)
            .do { message?.dismiss() }
            .subscribe(onError: { [weak self] error in

                self?.handleError(error)

            }) ?? Disposables.create()
    }

    private func handleError(_ error: Swift.Error) {

        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel))
        present(alert, animated: true)
    }
}

extension AuthorizationViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if let index = textFields.firstIndex(of: textField) {
            if textFields.count - 1 == index {
                view.endEditing(true)
            } else {
                textFields[index + 1]?.becomeFirstResponder()
            }
        }

        return true
    }
}
