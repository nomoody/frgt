//
//  NetworkController+Factory.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 24/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import RxSwift
import Swinject

private let logger = Logger()

extension NetworkController {

    final class Factory {

        static func register(into container: Container) {

            container.register(Interface.self) { _ in
                return Impl()
            }
        }
    }

    private class Impl: Interface {

        init(session: URLSession = .shared) {
            self.session = session
        }

        func send(request: URLRequest) -> Single<Data> {

            return send(request: request, expectedStatusCodes: [200])
        }

        func send(request: URLRequest, expectedStatusCodes: [Int]) -> Single<Data> {

            return .create { [weak self] single in

                let dataTask = self?.session.dataTask(with: request) { data, response, error in

                    #if DEBUG
                    sleep(3)
                    #endif

                    guard error == nil else {
                        logger.warning("Received response: '\(response?.url?.absoluteString ?? "")' with error='\(error?.localizedDescription ?? "")'.")
                        return single(.error(error!))
                    }

                    guard let httpResponse = response as? HTTPURLResponse else {
                        logger.warning("Received response: '\(response?.url?.absoluteString ?? "")' with unexpected body (can't be casted to HTTPURLResponse).")
                        return single(.error(Error.unexpectedResponse(response)))
                    }

                    logger.debug("Received response: '\(response?.url?.absoluteString ?? "")', statusCode='\(httpResponse.statusCode)'.")

                    guard expectedStatusCodes.contains(httpResponse.statusCode) else {

                        struct ResponseError: Decodable {
                            let error: Bool
                            let reason: String
                        }

                        if let apiError = try? JSONDecoder().decode(ResponseError.self, from: data ?? Data()) {

                            return single(.error(Error.apiReason(apiError.reason)))
                        }

                        return single(.error(Error.unexpectedStatusCode(httpResponse.statusCode)))
                    }

                    single(.success(data ?? Data()))
                }

                logger.debug("Request has been sent: '\(request.url?.absoluteString ?? "")'.")
                dataTask?.resume()

                return Disposables.create { dataTask?.cancel() }
            }
        }

        // MARK: - Private

        private let session: URLSession
    }
}
