//
//  NetworkController+URLBuilder.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 24/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import RxSwift
import Swinject

extension NetworkController {

    struct URLBuilder {

        static func signIn(with email: String, password: String) -> URLRequest {

            struct Body: Encodable { let email: String; let password: String }

            let url = baseURL.appendingPathComponent("authorize-me")
                .appendingPathComponent("sign-in")
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            let bodyForEncode = Body(email: email, password: password)
            request.httpBody = try? JSONEncoder().encode(bodyForEncode)

            return request
        }

        static func signUp(with email: String, password: String) -> URLRequest {

            struct Body: Encodable { let email: String; let password: String }

            let url = baseURL.appendingPathComponent("authorize-me")
                .appendingPathComponent("sign-up")
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            let bodyForEncode = Body(email: email, password: password)
            request.httpBody = try? JSONEncoder().encode(bodyForEncode)

            return request
        }

        static func signOut() -> URLRequest {

            struct Body: Encodable { let email: String; let password: String }

            let url = baseURL.appendingPathComponent("authorize-me")
                .appendingPathComponent("sign-out")
            var request = URLRequest(url: url)
            request.httpMethod = "POST"

            return request
        }

        static func checkToken() -> URLRequest {

            let url = baseURL.appendingPathComponent("authorize-me")
                .appendingPathComponent("checkToken")
            var request = URLRequest(url: url)
            request.httpMethod = "POST"

            return request
        }

        static func getAllNotes() -> URLRequest {

            let url = baseURL.appendingPathComponent("notes")
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            return request
        }

        static func saveNote(content: String) -> URLRequest {

            struct Body: Encodable { let content: String }

            let url = baseURL.appendingPathComponent("notes")
                .appendingPathComponent("create")
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            let bodyForEncode = Body(content: content)
            request.httpBody = try? JSONEncoder().encode(bodyForEncode)

            return request
        }

        static func updateNote(_ note: Note, with content: String) -> URLRequest {

            struct Body: Encodable { let id: Int; let content: String }

            let url = baseURL.appendingPathComponent("notes")
                .appendingPathComponent("update")
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            let bodyForEncode = Body(id: note.id, content: content)
            request.httpBody = try? JSONEncoder().encode(bodyForEncode)

            return request
        }

        static func deleteNote(_ note: Note) -> URLRequest {

            struct Body: Encodable { let id: Int; let content: String }

            let url = baseURL.appendingPathComponent("notes")
                .appendingPathComponent("delete")
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            let bodyForEncode = Body(id: note.id, content: note.content)
            request.httpBody = try? JSONEncoder().encode(bodyForEncode)

            return request
        }

        // MARK: - Private

        #if DEBUG
        private static let baseURL = URL(string: "http://localhost:8080")!
        #else
        private static let baseURL = URL(string: "https://api-frgt.herokuapp.com")!
        #endif
    }
}
