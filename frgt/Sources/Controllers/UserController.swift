//
//  UserController.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 24/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import RxSwift

struct UserController {

    typealias Interface = UserControllerInterface

    enum State: Equatable {

        case idle
        case noConnection
        case notAuthorized
        case authorized(_ user: User)

        static func == (lhs: UserController.State, rhs: UserController.State) -> Bool {
            switch (lhs, rhs) {

            case (.idle, .idle), (.notAuthorized, .notAuthorized), (.noConnection, .noConnection):
                return true

            case let (.authorized(userL), .authorized(userR)):
                return userL === userR

            default:
                return false
            }
        }
    }

    enum Error: Swift.Error {
        case userIsNotAuthorized
    }
}

protocol UserControllerInterface {

    var state: Observable<UserController.State> { get }

    func update()

    func signInWith(email: String, password: String) -> Completable
    func signUpWith(_ email: String, password: String) -> Completable
    func signOut() -> Completable

    func signRequest(_ request: inout URLRequest)
}
