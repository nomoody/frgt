//
//  KeychainController.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 01/05/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation

struct KeychainsController {

    typealias Interface = KeychainControllerInterface


    enum Key: String {
        case authorizedUser = "authorized-user"
    }
}

protocol KeychainControllerInterface {

    @discardableResult
    func save<T:Codable>(item: T, forKey key: KeychainsController.Key) -> Bool

    func get<T:Codable>(forKey key: KeychainsController.Key) -> T?

    @discardableResult
    func delete(forKey key: KeychainsController.Key) -> Bool
}
