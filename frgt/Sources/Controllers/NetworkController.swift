//
//  NetworkController.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 24/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import RxSwift

struct NetworkController {

    typealias Interface = NetworkControllerInterface

    enum Error: Swift.Error, LocalizedError {
        case unexpectedStatusCode(_ statusCode: Int)
        case unexpectedResponse(_ response: URLResponse?)
        case unexpectedContent
        case apiReason(_ reason: String)

        var errorDescription: String? {
            switch self {

            case let .apiReason(reason):
                return "Request has been failed by reason: '\(reason)'."

            case let .unexpectedStatusCode(code):
                return "Request has been failed with unexpected status code: '\(code)'."

            case let .unexpectedResponse(response):
                return "Request has been failed with unexpected response type: '\(response?.mimeType ?? "N/A")'."

            case .unexpectedContent:
                return "Request has been failed with unexpected content."
            }
        }
    }

    struct BaseResponse<Content: Decodable>: Decodable {

        let data: Content?
        let error: Bool?
        let reason: String?
    }
}

protocol NetworkControllerInterface {

    func send(request: URLRequest) -> Single<Data>
    func send(request: URLRequest, expectedStatusCodes: [Int]) -> Single<Data>
}
