//
//  UserController+Factory.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 24/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import RxSwift
import Swinject

private let logger = Logger()

extension UserController {

    final class Factory {

        static func register(into container: Container) {

            let threadSafeResolver = container.synchronize()
            container.register(Interface.self) { _ in
                return Impl(resolver: threadSafeResolver)
            }
            .inObjectScope(.container)
        }
    }

    private class Impl: Interface {

        init(resolver: Resolver) {

            logger.debug("UserController has been constructed.")
            self.networkController = resolver.resolve(NetworkController.Interface.self)!
            self.keychainsController = resolver.resolve(KeychainsController.Interface.self)!

            if let user: User = self.keychainsController.get(forKey: .authorizedUser) {
                checkToken(for: user)
            } else {
                stateSubj.onNext(.notAuthorized)
            }
        }

        deinit {
            logger.debug("~UserController has been destructed.")
        }

        var state: Observable<UserController.State> {
            return stateSubj
        }

        func update() {

            guard let state = try? stateSubj.value(), case let .authorized(user) = state else {
                stateSubj.onNext(.notAuthorized)
                return
            }

            checkToken(for: user)
        }

        func signInWith(email: String, password: String) -> Completable {

            let request = NetworkController.URLBuilder.signIn(with: email, password: password)

            return networkController.send(request: request)
                .map { [weak self] data in

                    let token = try JSONDecoder.makeEncoderWithDateFormatter()
                        .decode(User.Token.self, from: data)
                    let user = User(email: email, token: token)
                    self?.stateSubj.onNext(.authorized(user))
                    self?.keychainsController.save(item: user, forKey: .authorizedUser)
                }
                .asCompletable()
        }

        func signUpWith(_ email: String, password: String) -> Completable {

            let request = NetworkController.URLBuilder.signUp(with: email, password: password)

            return networkController.send(request: request)
                .map { [weak self] data in

                    let token = try JSONDecoder.makeEncoderWithDateFormatter()
                        .decode(User.Token.self, from: data)
                    let user = User(email: email, token: token)
                    self?.stateSubj.onNext(.authorized(user))
                    self?.keychainsController.save(item: user, forKey: .authorizedUser)
                }
                .asCompletable()
        }

        func signOut() -> Completable {

            var request = NetworkController.URLBuilder.signOut()
            signRequest(&request)

            return networkController.send(request: request)
                .do(onSuccess: { [weak self] _ in
                    self?.stateSubj.onNext(.notAuthorized)
                    self?.keychainsController.delete(forKey: .authorizedUser)
                })
                .asCompletable()
        }

        let networkController: NetworkController.Interface

        func checkToken(for user: User) {

            var request = NetworkController.URLBuilder.checkToken()
            setHttpHeaders(for: user, request: &request)

            checkTokenSubscription.disposable = networkController.send(request: request)
                .subscribe(onSuccess: { [weak self] data in

                    do {
                        let token = try JSONDecoder.makeEncoderWithDateFormatter()
                            .decode(User.Token.self, from: data)
                        user.token = token
                        self?.stateSubj.onNext(.authorized(user))
                        self?.keychainsController.save(item: user, forKey: .authorizedUser)

                    } catch {
                        logger.error("Check token request has been failed on serialization step with error='\(error.localizedDescription)'. Body: \(String(data: data, encoding: .utf8) ?? "'empty'").")
                        self?.stateSubj.onNext(.notAuthorized)
                        self?.keychainsController.delete(forKey: .authorizedUser)
                    }

                }) { [weak self] error in

                    logger.error("Check token request has been failed with error='\(error.localizedDescription)'.")
                    self?.stateSubj.onNext(.noConnection)
            }
        }

        func signRequest(_ request: inout URLRequest) {

            guard let state = try? stateSubj.value(), case let .authorized(user) = state else {
                logger.warning("Request hasn't been signed due to user is not authenticated.")
                return
            }

            setHttpHeaders(for: user, request: &request)
        }

        // MARK: - Private

        private let stateSubj = BehaviorSubject(value: State.idle)
        private let checkTokenSubscription = SerialDisposable()

        private let keychainsController: KeychainsController.Interface

        private func setHttpHeaders(for user: User, request: inout URLRequest) {

            request.allHTTPHeaderFields?["Content-Type"] = "application/json"
            request.allHTTPHeaderFields?["token"] = user.token?.token
            request.allHTTPHeaderFields?["identifier"] = "\(user.token?.accountId ?? 0)"
        }
    }
}
