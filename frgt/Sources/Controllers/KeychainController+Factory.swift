//
//  KeychainController+Factory.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 01/05/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation
import KeychainAccess
import Swinject

private let logger = Logger()

extension KeychainsController {

    final class Factory {

        static func register(into container: Container) {

            container.register(Interface.self) { _ in
                return Impl()
            }
        }
    }

    private class Impl: Interface {

        func save<T: Codable>(item: T, forKey key: KeychainsController.Key) -> Bool {

            logger.debug("Storing '\(item)' into Keychains...")
            do {
                let data = try JSONEncoder().encode(item)
                try keychain.set(data, key: key.rawValue)
                logger.debug("Storing user into Keychains has been successful.")
            } catch {
                logger.debug("Storing user into Keychain has been failed with error='\(error.localizedDescription)'.")
                return false
            }

            return true
        }

        func get<T: Codable>(forKey key: KeychainsController.Key) -> T? {

            guard let data = try? keychain.getData(key.rawValue) else {
                logger.debug("Keychain has no '\(key)' data.")
                return nil
            }

            do {
                return try JSONDecoder().decode(T.self, from: data)
            } catch {
                logger.debug("Decode has been failed with error='\(error.localizedDescription)'. Removing...")
                delete(forKey: key)
                return nil
            }
        }

        @discardableResult
        func delete(forKey key: KeychainsController.Key) -> Bool {

            logger.debug("Deleting Keychains data...")
            do {
                try keychain.remove(key.rawValue)
                logger.debug("Deleting has been successfully.")
            } catch {
                logger.debug("Deleting has been failed with error='\(error.localizedDescription)'.")
                return false
            }

            return true
        }

        // MARK: - Private

        private let keychain = Keychain(service: Bundle.main.bundleIdentifier ?? "com.dukhovnyi.frgt")
    }
}
