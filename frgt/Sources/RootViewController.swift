//
//  RootViewController.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 20/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        coordinator.present()
    }

    // MARK: - Private

    private lazy var coordinator = RootNavigator(viewController: self)
}

