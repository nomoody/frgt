//
//  Appearance.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 27/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import UIKit

struct Appearance {

    static let shared = Appearance.predefined

    let backgroundColor: UIColor
    let textColor: UIColor
    let highlightedTextColor: UIColor

    let buttonColor: UIColor
    let alternativeButtonColor: UIColor

    let editor: Editor
}

extension Appearance {

    static let predefined = Appearance(
        backgroundColor: UIColor(0x293850),
        textColor: .white,
        highlightedTextColor: UIColor(0x984063),
        buttonColor: UIColor(0xFE9677),
        alternativeButtonColor: .clear,

        editor: .system
    )
}

struct Editor {

    struct Font {

        let regular: UIFont
        let bold: UIFont
        let light: UIFont

        init(name: String, size: CGFloat) {

            let font = UIFont(name: name, size: size) ?? UIFont.systemFont(ofSize: size)

            regular = font
            bold = font.with(.traitBold)
            light = font.with(.traitItalic)
        }
    }

    let font: Font

    static let system = Editor(font: Font(name: "Helvetica", size: 19))
}

extension UIFont {

    func with(_ traits: UIFontDescriptor.SymbolicTraits...) -> UIFont {
        guard let descriptor = self.fontDescriptor.withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits).union(self.fontDescriptor.symbolicTraits)) else {
            return self
        }
        return UIFont(descriptor: descriptor, size: 0)
    }

    func without(_ traits: UIFontDescriptor.SymbolicTraits...) -> UIFont {
        guard let descriptor = self.fontDescriptor.withSymbolicTraits(self.fontDescriptor.symbolicTraits.subtracting(UIFontDescriptor.SymbolicTraits(traits))) else {
            return self
        }
        return UIFont(descriptor: descriptor, size: 0)
    }
}
