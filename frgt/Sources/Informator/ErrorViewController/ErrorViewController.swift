//
//  ErrorViewController.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 13/05/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import UIKit

class ErrorViewController: UIViewController {

    static let storyboardId = String(describing: ErrorViewController.self)

    var onClose: () -> Void = {}

    var error: Error? {
        didSet {
            updateUI()
        }
    }

    override func viewDidLoad() {

        super.viewDidLoad()

        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTouchUpInside)))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        updateUI()
    }

    // MARK: - IBOutlets

    @IBOutlet weak var errorLabel: UILabel?

    // MARK: - Private

    private func updateUI() {

        errorLabel?.text = error?.localizedDescription
    }

    @objc private func viewTouchUpInside(_ gestureRecognizer: UITapGestureRecognizer) {

        onClose()
    }
}
