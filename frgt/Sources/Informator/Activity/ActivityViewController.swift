//
//  ActivityViewController.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 06/05/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import UIKit

class ActivityViewController: UIViewController {

    static let storyboardId = String(describing: ActivityViewController.self)
}
