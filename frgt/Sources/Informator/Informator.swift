//
//  Informator.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 06/05/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import UIKit

private let logger = Logger()

final class Informator {

    enum Content {
        case activityIndicator
        case error(_ error: Error)
    }

    struct Message {

        let identifier = UUID()

        init(_ onDismiss: @escaping () -> Void) {
            self.onDismiss = onDismiss
        }

        func dismiss() {
            onDismiss()
        }

        // MARK: - Private

        private let onDismiss: () -> Void
    }

    func show(_ content: Content) -> Message? {

        var message: Message? = nil

        withRootViewController { viewController in

            let childViewController = makeViewController(for: content)
            viewController.add(childViewController)

            let onDismiss = childViewController.remove

            if let errorViewController = childViewController as? ErrorViewController {
                errorViewController.onClose = onDismiss
            }

            message = Message(onDismiss)
        }

        return message
    }

    // MARK: - Private

    private let storyboard = UIStoryboard(name: "Informator", bundle: nil)

    private func withRootViewController(_ completion: (_ viewController: UIViewController) -> ()) {

        guard let viewController = UIApplication.shared.keyWindow?.rootViewController else {
            logger.error("Can't get key window.")
            return
        }

        completion(viewController)
    }

    private func makeViewController(for content: Content) -> UIViewController {

        switch content {
        case .activityIndicator:
            return makeActivityViewController()
        case let .error(error):
            return makeErrorViewController(with: error)
        }
    }

    private func makeActivityViewController() -> ActivityViewController {

        let viewController = storyboard.instantiateViewController(withIdentifier: ActivityViewController.storyboardId)
            as! ActivityViewController

        return viewController
    }

    private func makeErrorViewController(with error: Error) -> ErrorViewController {

        let viewController = storyboard.instantiateViewController(withIdentifier: ErrorViewController.storyboardId)
            as! ErrorViewController
        viewController.error = error

        return viewController
    }
}
