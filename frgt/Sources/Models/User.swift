//
//  User.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 24/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation

final class User: Codable {

    struct Token: Codable {
        let token: String
        let accountId: Int

        let createdAt: Date?
        let updatedAt: Date?
        let expiredAt: Date?
    }

    let email: String
    var token: Token?

    init(email: String, token: Token? = nil) {
        self.email = email
        self.token = token
    }
}
