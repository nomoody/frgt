//
//  Note.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 07/04/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation

// "[{\"id\":2,\"content\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry...\",\"accountId\":1}]"

final class Note: Decodable {

    let id: Int
    var content: String
    let ownerId: Int
    let updatedAt: Date
}

extension Note {

    static let Decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }()
}
