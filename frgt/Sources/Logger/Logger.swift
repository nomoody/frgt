//
//  Logger.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 24/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Foundation

final class Logger {

    func debug(_ message: String) {

        log(message, level: .debug)
    }

    func warning(_ message: String) {

        log(message, level: .warning)
    }

    func error(_ message: String) {

        log(message, level: .error)
    }

    // MARK: - Private

    private enum Level: String {
        case debug = "ℹ️"
        case warning = "⚠️"
        case error = "⛔️"
    }

    private func log(_ message: String, level: Level) {

        let now = self.dateFormatter.string(from: Date())
        print("\(now): \(level.rawValue): \(message)")
    }

    private lazy var dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "HH:mm:ss.SSS"
        return df
    }()
}
