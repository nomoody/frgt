//
//  AppDelegate.swift
//  frgt
//
//  Created by Yurii Dukhovnyi on 20/03/2019.
//  Copyright © 2019 Yurii Dukhovnyi. All rights reserved.
//

import Swinject
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    override init() {

        let container = Container.Default.container

        NetworkController.Factory.register(into: container)
        NotesViewModel.Factory.register(into: container)
        KeychainsController.Factory.register(into: container)
        UserController.Factory.register(into: container)
    }
}

